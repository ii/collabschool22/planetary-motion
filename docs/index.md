# Planetary motions
Here we simulate planetary motions in 2D space.

## Summary of components

  * [main.py](main.md) : The application runs from here!
  * [parse_config.py](parse_config.md) : Parser for configurations files.
  * [simulation.py](simulation.md) : The simulation runs here
  * [renderer.py](renderer.md) : The simulation is visualized here!
    