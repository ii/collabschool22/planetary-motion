import matplotlib.pyplot as plt
import matplotlib.animation as anm
import matplotlib.cm as cm
import math
import numpy as np
import time

class FuncAnimationDisposable(anm.FuncAnimation):

  """
  A class used to fix the broken FuncAnimation class: problem when closing figure
  See [https://stackoverflow.com/questions/17446956/matplotlib-exit-after-animation](https://stackoverflow.com/questions/17446956/matplotlib-exit-after-animation)

  """

  def __init__(self, fig, func, **kwargs):
    super().__init__(fig, func, **kwargs)
    
  def _step(self, *args):
    still_going = anm.Animation._step(self, *args)
    if not still_going and self.repeat:
      super()._init_draw()
      self.frame_seq = self.new_frame_seq()
      self.event_source.interval = self._repeat_delay
      return True
    elif (not still_going) and (not self.repeat):
      plt.close()
      return False
    else:
      self.event_source.interval = self._interval
      return still_going
    
  def _stop(self, *args):
    # On stop we disconnect all of our events.
    if self._blit:
      self._fig.canvas.mpl_disconnect(self._resize_id)
    self._fig.canvas.mpl_disconnect(self._close_id)

class Renderer:

  """
  A class used to render the multi-body problem

  Attributes:
    simulation (Simulation): Physics simulation class
    tailsize (int): Number of data points to visualize per trajectory
    stepsize (int): Number of steps between visualization updates
    ax (object): Axis object handled internally
    fig (object): Figure object handled internally
    animation (object): Animation object handled internally
    tails (object): Line plots representating last tailsize positions for every trajectory
    heads (object): Scatter plot representing last position for every trajectory
    tail_positions (nd.array): Array of shape (num_planets, 2, tailsize) storing last tailsize positions for every trajectory
    head_positions (nd.array): Array of shape (num_planets, 2) storing last position for every trajectory
  """

  def __init__(self, simulation, xlim, ylim, tailsize=1, stepsize=1, figsize=None, interval=100, repeat=bool(0)):
    self.simulation = simulation
    self.tailsize = tailsize
    self.stepsize = stepsize
    self.init_positions()
    self.set_window(xlim, ylim, figsize)
    self.set_animation(interval, repeat)

  def set_window(self, xlim, ylim, figsize):
    self.fig, self.ax = plt.subplots(1, 1, figsize=figsize)
    self.ax.set_xticks([])
    self.ax.set_yticks([])
    self.ax.set_facecolor("k")
    self.ax.set_aspect("equal")
    self.ax.set_xlim(xlim)
    self.ax.set_ylim(ylim)

  def set_animation(self, interval, repeat):
    empty_scatter = np.full(shape=(self.simulation.num_planets,), fill_value=np.nan)
    planets_cmap = cm.cool(np.linspace(0, 1, self.simulation.num_planets))
    self.tails = [self.ax.plot([], [], c=planets_cmap[i], linewidth=3, zorder=1)[0] for i in range(self.simulation.num_planets)]
    self.heads = self.ax.scatter(empty_scatter, empty_scatter, c=planets_cmap, s=100*self.simulation.masses, edgecolors=np.array([1.0, 1.0, 1.0]), zorder=2)
    self.animation = FuncAnimationDisposable(fig=self.fig, 
                         func=self.update_renderer,
                         frames=self.simulation.num_itrs,
                         interval=interval,
                         repeat=repeat)

  def init_positions(self): # add init positions !!!
    self.tail_positions = np.full(shape=(self.simulation.num_planets, 2, self.tailsize), fill_value=np.nan)
    self.head_positions = np.full(shape=(self.simulation.num_planets, 2), fill_value=np.nan)

  def new_positions(self, frame):
    nan_columns = np.all(np.isnan(self.tail_positions[:,0,:]), axis=0)
    self.simulation.step()
    self.head_positions = self.simulation.coordinates
    if nan_columns.sum() > 0: # at least a dummy column found, fill in first such column:
      self.tail_positions[:,:,np.where(nan_columns)[0][0]] = self.head_positions
    else: # all columns are filled with true coordinates, fill in FIFO style:
      self.tail_positions[:,:,:-1] = self.tail_positions[:,:,1:]
      self.tail_positions[:,:,-1] = self.head_positions

  def update_renderer(self, frame):
    # Updade physics:
    self.new_positions(frame)
    scale_factor = self.simulation.zoom_scale_factor()
    # Update renderer:
    if frame % self.stepsize == 0:
      for i, tail in enumerate(self.tails):
        tail.set_data(self.tail_positions[i,0,:], self.tail_positions[i,1,:])
        tail.set_linewidth(scale_factor*3)
      self.heads.set_offsets(self.head_positions)
      self.heads.set_sizes(scale_factor*100*self.simulation.masses)
      self.ax.set_title(f"Frame {frame}")
      xlim, ylim = self.simulation.padded_limits(0.1)
      self.ax.set_xlim(xlim)
      self.ax.set_ylim(ylim)

  def show(self):
    """Shows the rendered simulation
    """    
    t0 = time.monotonic()
    plt.show()
    t1 = time.monotonic()
    print(f"Elapsed time: {t1 - t0}")

  def save(self):
    """Saves the rendered simulation
    """
    t0 = time.monotonic()
    self.animation.save(f"Physics_simulation_{self.simulation.num_planets}_planets.mp4")
    t1 = time.monotonic()
    print(f"Elapsed time: {t1 - t0}")






class DummySimulation():
  def __init__(self, num_planets, num_itrs, seed):
    if seed is not None:
      np.random.seed(seed)
    self.num_planets = num_planets
    self.num_itrs = num_itrs
    self.masses = np.random.rand(self.num_planets)
    self.coordinates = np.zeros(shape=(num_planets, 2))
    self.count = 0
    self.init_dummy_trajectories()

  def init_dummy_trajectories(self):
    theta = np.linspace(0, 2*math.pi, self.num_itrs)
    self.dummy_trajectories = np.zeros(shape=(self.num_planets, 2, self.num_itrs))
    self.dummy_trajectories[0,0,:] = np.cos(theta)
    self.dummy_trajectories[0,1,:] = np.sin(theta)
    for k in range(1, self.num_planets):
      self.dummy_trajectories[k,0,:] = k * np.flip(self.dummy_trajectories[k-1,0,:]) / (k+1)
      self.dummy_trajectories[k,1,:] = k * np.flip(self.dummy_trajectories[k-1,1,:]) / (k+1)

  def step(self):
    self.coordinates = self.dummy_trajectories[:,:,self.count]
    self.count += 1
    if self.count == self.num_itrs:
      self.count = 0


# dummy_simulation = DummySimulation(num_planets=5, num_itrs=1000, seed=0) #-> put in test file
# renderer = Renderer(simulation=dummy_simulation, xlim=[-1.0, 1.0], ylim=[-1.0, 1.0], tailsize=100, stepsize=1, figsize=(8, 8), interval=20, repeat=bool(0))
# renderer.show()
# renderer.save()


# TODO:
# write tests -> focus on parsing tests, + test saving works