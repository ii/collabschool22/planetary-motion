import math
import numpy as np

class Simulation:
  """ Represents a physics simulation of orbiting particles.

    Attributes:
      masses (list): List of masses of planets
      globals (dict): Global variables read through config
      planets (list): List of planets
      num_planets (int): Number of planets
      num_itrs (int): Number of iterations
      coordinates (ndarray[num_planets, 2]): 2D array of coordinates
  """  
  def __init__(self, configuration, num_itrs=10000):
    """ Initializes a new simulation

    Args:
        configuration (object): Configuration of the simulation. The key 'global' gives global variables, and the key 'planets' gives a list of planets and according properties
        num_itrs (int, optional): Number of iterations to run. Defaults to 10000.
    """    
    self.globals = configuration['global']
    self.planets = configuration['planets']
    self.num_planets = len(self.planets)
    self.num_itrs = num_itrs
    self.t = 0.1 if 't' not in self.globals else self.globals['t']

    self._last_span = 0
    self._initialize_arrays()
    self._compute_initial_limits()
    self._update_arrays()

  def padded_limits(self, padding=0.3):
    """Computes the padded limits

    Args:
        padding (float, optional): percentage of padding to use. Defaults to 0.3.

    Returns:
        (xlim, ylim): the padded x and y limits around the space
    """
    xs = np.array([p['x'] for p in self.planets])
    ys = np.array([p['y'] for p in self.planets])

    cx = self.cx_0
    cy = self.cy_0

    ds = np.sqrt(((xs - cx) ** 2, (ys - cy) ** 2))
    span = np.max(ds)
    self.r = span

    if span > self._last_span:
      self._last_span = span
      return (
        [cx - span * (1.0 + padding), cx + span * (1.0 + padding)],
        [cy - span * (1.0 + padding), cy + span * (1.0 + padding)],
      )
    else:
      return (
        [cx - self._last_span * (1.0 + padding), cx + self._last_span * (1.0 + padding)],
        [cy - self._last_span * (1.0 + padding), cy + self._last_span * (1.0 + padding)],
      )

  def zoom_scale_factor(self):
    r0 = self.r_0
    r1 = self.r

    return min(r0 / r1, 1.7)

  def _compute_initial_limits(self):
    xs = np.array([p['x'] for p in self.planets])
    ys = np.array([p['y'] for p in self.planets])

    cx = np.mean(xs)
    cy = np.mean(ys)

    ds = np.sqrt(((xs - cx) ** 2, (ys - cy) ** 2))

    self.r_0 = np.max(ds)
    self.r = self.r_0
    self.cx_0 = cx
    self.cy_0 = cy
    self.min_x = min(xs)
    self.max_x = max(xs)
    self.min_y = min(ys)
    self.max_y = max(ys)

  def _initialize_arrays(self):
    # Compute normalized masses
    masses = [p['mass'] for p in self.planets]
    max_mass = max(masses)
    self.masses = np.array([m / max_mass for m in masses])
    
    # Sets the x, y coords to initial coords
    for p in self.planets:
      p['x'] = p['x_0']
      p['y'] = p['y_0']

    self.coordinates = np.zeros(shape=(len(self.planets), 2))


  def _evaluate(self, a, b):
    distance = math.sqrt((a['x'] - b['x']) ** 2 + (a['y'] - b['y']) ** 2)
    dx = b['x'] - a['x']
    dy = b['y'] - a['y']
    return dx, dy, distance

  def _update_arrays(self):
    for i, p in enumerate(self.planets):
      self.coordinates[i, 0] = p['x']
      self.coordinates[i, 1] = p['y']

  def step(self):
    """
      Steps the iteration by one timestep defined by self.t
    """
    t = self.t

    G = self.globals['G']
    for A in self.planets:
      ax = 0
      ay = 0
      for B in self.planets:
        if A != B: 
          prod_mass = A['mass'] * B['mass']
          dx, dy, distance = self._evaluate(A, B)
          
          scale = G * prod_mass / distance**3
          fx = dx * scale
          fy = dy * scale

          ax += fx / A['mass']
          ay += fy / A['mass']
          
      A['vx'] += ax * t
      A['vy'] += ay * t
      
      A['x'] += A['vx'] * t
      A['y'] += A['vy'] * t
    
    self._update_arrays()
