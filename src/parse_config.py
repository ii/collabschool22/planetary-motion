def parse(path):
  """Parses a config file with a given path

  Args:
      path (str): the path to the config file

  Examples:
  ``` py
    {
      'global': { 'G': 10 },
      'planets': [ 
        { 'name': '13ui9jdwqo',
          'mass': 10,
          'vx': 6,
          'vy': 4,
          'x_0': 50,
          'y_0': 29 
        }, 
        { 'name': '1Zieohoj',
          'mass': 10,
          'vx': 2,
          'vy': 3,
          'x_0': 11,
          'y_0': 22 
        } 
      ] 
    }
  ```
    

  Returns:
      parsed_file (object): An object representation of the configuration, where the key 'global' gives the list of global variables, and the key 'planets' gives a list of planets with according properties
  """

  result = {
    'global': {},
    'planets': []
  }

  with open(path, 'r') as file:
    lines = []
    
    # Remove line breaks and trailing/initial spaces
    for line in file:
      line = line.strip().replace('\n', '')

      if line == '': continue
      else: lines.append(line)
    
    def is_header(line): 
      return line.startswith('[') and line.endswith(']')
    
    def parse_entry(entry):
      """ Parses a key,value (ex: a, b) tuple from an entry (ex a = b)"""
      key, val = [x.strip() for x in entry.split('=')]
      if key not in ['name']:
        val = float(val)
      
      return key, val

    # Parse specific configurations per header, and store accordingly
    header_indices = [i for (i, x) in enumerate(lines) if is_header(x)] 
    for i in header_indices:
      # Create the key to appear in parsed file
      # without away '[', ']', and set to lower case
      global_key = lines[i].replace('[', '').replace(']', '').lower()
      
      start = i + 1
      end = i + 2 # Find the end index
      while end < len(lines) and not is_header(lines[end]):
        end += 1

      info = {}

      # Parse specific parameters
      for j in range(start, end):
        key, value = parse_entry(lines[j])
        info[key] = value

      if global_key == 'global':
         # Add global config
        result[global_key] = info
      else: 
        # Add planet
        result['planets'].append(info)

  return result