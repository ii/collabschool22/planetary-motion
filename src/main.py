from parse_config import parse
from simulation import Simulation
from renderer import Renderer
import pathlib
import argparse
import os

print(__file__)

def start(configfile):
  """Starts the planetary simulation with a given configuration file

  Args:
      configfile (str): name of the config file
  """
  configuration = parse(configfile)
  simulation = Simulation(configuration)
  xlim, ylim = simulation.padded_limits()
  renderer = Renderer(simulation, xlim=xlim, ylim=ylim, tailsize=100, stepsize=1, figsize=(8, 8), interval=20, repeat=bool(1))
  renderer.show()

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--method", default="rungakutta", type=str, choices=['rungakutta', 'euler'], help=" calculation methods being either rungakutta or euler")
  parser.add_argument("--configfile", default="configfile2.txt", type=str, help=" name of configfile")
  args = parser.parse_args()
  start(configfile=pathlib.Path(os.getcwd(), args.configfile))


