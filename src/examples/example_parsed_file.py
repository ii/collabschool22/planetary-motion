"""
  This is an example output of file parsing.
"""
example_parsed_file = {
  'global': { 'G': 10 },
  'planets': [
    { 
      'name': '13ui9jdwqo',
      'mass': 10,
      'vx': 6,
      'vy': 4,
      'x_0': 50,
      'y_0': 29,
    },
    { 
      'name': 'dsadklwqB',
      'mass': 10,
      'vx': 0,
      'vy': 3,
      'x_0': 33,
      'y_0': 22, 
    },
    { 
      'name': 'dsamwoqmC',
      'mass': 10,
      'vx': 1,
      'vy': 0,
      'x_0': 11,
      'y_0': 295
    },
    { 
      'name': 'dwqmklmkwD',
      'mass': 10,
      'vx': 2,
      'vy': 0,
      'x_0': 5,
      'y_0': 50 
    },
    { 
      'name': 'Esdajjdals',
      'mass': 10,
      'vx': 6,
      'vy': 4,
      'x_0': 5,
      'y_0': 26 
    },
  ],
}