from pathlib import Path
import os
from .parse_config import parse
from .examples import example_parsed_file

example_file_path = os.path.join(Path.cwd(), "src", "examples", "example_file.txt")

def test_parse_example_file():
  """ 
    Asserts that the example file is parsed into a
    correctly corresponding object.
  """
  actual = parse(example_file_path)
  expected = example_parsed_file.example_parsed_file
  assert actual == expected